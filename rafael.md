RAFA

Username: docker
password: docker

Ready
```
----------------------------------------------------
number	NAME	EXTERNAL_IP
1	docker-workshop-1ng5	34.77.124.104
2	docker-workshop-1r4f	35.233.8.71
3	docker-workshop-2m4w	35.241.227.1
4	docker-workshop-37v4	35.195.193.211
5	docker-workshop-3h17	34.76.27.102
6	docker-workshop-527r	104.155.38.183
7	docker-workshop-5pnk	146.148.2.168
8	docker-workshop-6mr8	35.240.10.56
9	docker-workshop-72gl	34.77.121.164
10	docker-workshop-9963	35.241.234.87
11	docker-workshop-9tp6	35.205.162.190
12	docker-workshop-bh1k	35.205.144.210
13	docker-workshop-c666	34.76.86.67
14	docker-workshop-cw64	34.77.144.235
15	docker-workshop-fwc7	35.189.192.51
16	docker-workshop-kgzp	104.199.74.163
17	docker-workshop-lc05	35.187.46.14
18	docker-workshop-sdwn	35.241.170.177
19	docker-workshop-wp2k	35.205.195.39
20	docker-workshop-x9n9	35.187.174.113
21	docker-workshop-xcx3	130.211.100.137
22	docker-workshop-z37v	35.233.28.217
----------------------------------------------------
```
