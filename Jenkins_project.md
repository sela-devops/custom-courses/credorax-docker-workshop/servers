Medical case

Please create a new directory named nested_docker

```
sudo mkdir ~/nested_project
```

Enter to the folder
```
sudo cd ~/nested_project
```

Create docker compose file
```
sudo nano ~/nested_project/docker-compose.yml
```

Insert this text into the compose file & save
```
version: "3.7"
services:
  jenkins:
    image: "jenkins_with_docker"
    build:
      context: ./jenkins
    restart: always
    volumes:
    - jenkins_home:/var/jenkins_home
    - /var/run/docker.sock:/var/run/docker.sock

    ports:
    - "8080:8080"
    - "50000:50000"
    privileged: true

volumes:
  jenkins_home:
    external: true
```

create jenkins folder
```
sudo mkdir ~/nested_project/jenkins
```

Enter to the folder
```
sudo cd ~/nested_project/jenkins
```

Download specific docker version
```
sudo wget https://download.docker.com/linux/static/stable/x86_64/docker-18.06.3-ce.tgz
```
create a docker volume for jenkins
```
docker volume create --name=jenkins_home
```
Create docker file
```
sudo nano ~/nested_project/jenkins/Dockerfile
```

Insert this text into the Dockerfile & save
```
FROM jenkins/jenkins:lts
USER root
RUN apt-get update && apt-get install -y sudo
COPY ./docker-18.06.3-ce.tgz /tmp
RUN tar -xvf /tmp/docker-18.06.3-ce.tgz -C /tmp && mv /tmp/docker/docker /usr/bin/docker && rm -f /tmp/docker-18.06.3-ce.tgz
RUN usermod -aG sudo jenkins
RUN echo "jenkins ALL=(ALL:ALL) NOPASSWD:ALL"  >> /etc/sudoers
USER jenkins
```
now make sure you are in the nested folder
```
cd ~/nested_project
```

Lunch the docker compose !
```
sudo docker-compose up
```

In the docker compose console
Find **YOUR** number Bolded in the screen and and copy it to a notepad
``` 
jenkins_1  | Please use the following password to proceed to installation:
jenkins_1  |
jenkins_1  | ** 8fa6a68a046c423c80b5fd25bb94eca0 **
jenkins_1  |
jenkins_1  | This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
```

Enter the text below to your browser
```
http://<your-IP>:8080
```
On jenkins :

1. on the secret part enter the secret on your notepad
2. on install plugins
    - click on : "install suggested plugin"

3. continue as admin
4. on Instance Configuration
    - click on : "save and finish"
5. create new item
    - give a name to the project
    - choose pipeline project
6. go to pipeline tab and insert the following code
```
pipeline {
    agent any
    stages {
        stage('git clone') {
            steps {
                script {
                    sh "git clone https://github.com/prakhar1989/docker-curriculum.git"
                }
            }
        }
        
        stage('docker build') {
            steps {
                script {
                    dir("docker-curriculum/flask-app") {
                        // build the flask container
                        sh "sudo docker build -t doda/catnip ."
                    }
                }
            }
        }
        stage('docker run container on host via socket') {
            steps {
                script {
                    // start the flask app container
                    sh "sudo docker run -d -p 8888:5000 doda/catnip"
                }
            }
        }
    }
    post { 
        always { 
            cleanWs()
        }
    }
}

```
7. save the pipeline
8. Build IT!
9. Once you got SUCCESS status check your browser on port 8888
```
http://<your-ip>:8888
```
Amazing!!! great job!
