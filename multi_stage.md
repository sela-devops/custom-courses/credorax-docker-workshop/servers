Please create a new directory named Multi

```
sudo mkdir ~/Multi
```

Enter to the folder
```
cd ~/Multi
```

clone the project
```
sudo git clone https://github.com/bbachi/docker-multibuild-example.git
```

Enter to the folder docker-multibuild-example
```
cd docker-multibuild-example
```

Please build the full docker file
```
sudo docker build -t nodewebapp:v1 -f ./Dockerfile.dev .
```

Please run the application from the image nodewebapp:v1
```
sudo docker run -it -d --name nodewebapp1  -p 3070:3070 nodewebapp:v1
```
Enter the text below to your browser
```
http://<your-IP>:3070
```

Please build the full docker multi stage file
```
sudo docker build -t nodewebapp:v2 -f ./Dockerfile .
```

Please run the application from the nodewebapp1 nodewebapp:v2
```
sudo docker run -it -d --name nodewebapp2 -p 3071:3070 nodewebapp:v2
```

Enter the text below into your browser and make sure that it is the same application
```
http://<your-IP>:3071
```

Please compare the size of the docker images by size
```
sudo docker images
```

Please compare the RAM usage of the docker containers
```
sudo docker stats nodewebapp1 nodewebapp2 
```

Clean the environment
```
docker rm  $(docker ps -a -q)
docker rmi $(docker images -a -q)
```