**Hello Rafal Friends**!

For this project, you need to create a Dockerfile, a Python dependencies file, and a docker-compose.yml 

Create an empty project directory named djano.
```
sudo mkdir django
```

Enter the folder
```
cd django
```

This directory will be the context for your application image.

Create a new Dockerfile called "Dockerfile" in your project directory.

Add the following content to the Dockerfile.
```
FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
```
Save and close the Dockerfile.

This Dockerfile starts with a Python 3 parent image.

The parent image is modified by adding a new code directory.

The parent image is further modified by installing the Python requirements defined in the requirements.txt file.

Create a requirements.txt in your project directory.
This file is used by the RUN pip install -r requirements.txt command in your Dockerfile.
```
sudo nano ~/django/requirements.txt
```

Add the required software in the file.
```
Django>=2.0,<3.0
psycopg2>=2.7,<3.0
```
Save and close the requirements.txt file.

Create a file called docker-compose.yml in your project directory.
```
sudo nano ~/django/docker-compose.yml
```

Add the following configuration to the file.
```
version: '3'

services:
  db:
    image: postgres
  web:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db
```
Save and close the docker-compose.yml file.

The docker-compose.yml file describes the services that make your app. 
In this example those services are a web server and database. 
The compose file also describes: 
- which Docker images these services use.
- how they link together.
- any volumes they might need mounted inside the containers.
- and Finally, the docker-compose.yml file describes which ports these services expose. 


Our file specificly comprised from two services: 
- The db service 
- the web service.


Create a Django project:
In this part, we create a Django starter project by building the image from the build context defined in the previous procedure.

Create the Django project by running the docker-compose run command as follows.
```
sudo docker-compose run web django-admin startproject composeexample .
```
This instructs Compose to run django-admin startproject composeexample in a container, using the web service’s image and configuration. 
Because the web image doesn’t exist yet, Compose builds it from the current directory, as specified by the build: . line in docker-compose.yml.

Once the web service image is built, Compose runs it and executes the django-admin startproject command in the container.
This command instructs Django to create a set of files and directories representing a Django project.

After the docker-compose command completes, list the contents of your project.
```
sudo ls -l
```
You should see the next output
```
drwxr-xr-x 2 root   root   composeexample
-rw-rw-r-- 1 user   user   docker-compose.yml
-rw-rw-r-- 1 user   user   Dockerfile
-rwxr-xr-x 1 root   root   manage.py
-rw-rw-r-- 1 user   user   requirements.txt
```

Because we are running Docker on Ubntu Linux on our SELA course, the files django-admin created are owned by root. 
This happens because the container runs as the root user.

**!!!** Bonus! please discover how can we change the user the container runs from within and tell me (Ohr) ***!!!**

Change the ownership of the new files.
```
sudo chown -R $USER:$USER .
```

In this section, we set up the database connection for Django.

Edit the ~/django/composeexample/settings.py file.

Replace the DATABASES = ... with the following:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```
Save and close the file.

These settings are determined by the postgres Docker image specified in docker-compose.yml.


Run the docker-compose up command from the top level directory for your project.
```
sudo docker-compose up -d
```

now the watch the process with docker compose logs
```
sudo docker-compose logs
```

you will see output similer to that:
```
djangosample_db_1 is up-to-date
Creating djangosample_web_1 ...
Creating djangosample_web_1 ... done
Attaching to djangosample_db_1, djangosample_web_1
db_1   | The files belonging to this database system will be owned by user "postgres".
db_1   | This user must also own the server process.
db_1   |
db_1   | The database cluster will be initialized with locale "en_US.utf8".
db_1   | The default database encoding has accordingly been set to "UTF8".
db_1   | The default text search configuration will be set to "english".

. . .

web_1  | May 30, 2017 - 21:44:49
web_1  | Django version 1.11.1, using settings 'composeexample.settings'
web_1  | Starting development server at http://0.0.0.0:8000/
web_1  | Quit the server with CONTROL-C.
```
At this point, your Django app should be running at port 8000 on your Docker host.
please go to your browser and watch your website
```
http://<your-ip>:8000
```

**Ohhhhhh no !!!!!!! it is not working !!!!! why beloved lecturer? why did you lie to us!!!**
dont worry it is part of the plan :)

Edit the ~/django/composeexample/settings.py file again

Replace the ALLOWED_HOSTS = [] with the following:
```
ALLOWED_HOSTS = ['*']
```
Now refresh ypur browser page !

Amazing! see how independent and responsive the the compose stack is no changes were needed in the compose itself

📞📞📞📞📞📞  
Wait a minute i have phone call..
"aha yap... yes ... i see .. ok sure thing ... bye"

Listen guys i just got a phone call from your security-BAMAM team and they are "nagging" me that port 8000 is unsecured
Apperantly port 9000 is much safer!

Go to the compose file and change the stack to work with port 9000 instead of 8000
```
I will not spoon feed you, i guess you know what to do by now :)
```

After your done please run
```
sudo docker-compose up -d
```
Oberve how only the **web** container changed, the compose mechanism is loosly coupled and knows to detect changes
```
docker@docker-workshop-7f8j:~/bla$ docker-compose up -d
bla_db_1 is up-to-date
Recreating bla_web_1 ... done
```
Go to your browser and watch your website again
```
http://<your-ip>:9000
```

Great Job!